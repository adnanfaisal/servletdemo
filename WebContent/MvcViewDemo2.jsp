<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<style type="text/css" media="all">

h2 {
background-color:green;
}

</style>

</head>
<body>

<p>
<h2> Student Information</h2>
<table border="1">
<thead>
<th> First Name </th>
<th> Last Name </th>
<th> E-mail </th>
</thead>
<tbody>
<c:forEach var="student" items="${studentList}">
 <tr>
 	<td> ${student.firstName}
 	<td> ${student.lastName}
 	<td> ${student.email}    
 </tr>
</c:forEach>
</tbody>
</table>
</body>
</html>