package com.adnan.servletdemo.mvc2;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the helper class for the Model 
 * @author adnan
 *
 */
public class StudentDataUtil {

	public List<Student> getStudentList(){
		
		List <Student> students = new ArrayList<>();
		
		students.add(new Student("Adnan","Faisal","adnanfaisal@gmail.com"));
		students.add(new Student("Farhana","Islam","farhanaislam@gmail.com"));
		students.add(new Student("Sharlock","Holmes","sharlock@gmail.com"));
		
		return students;
		
	}
}
