package com.adnan.servletdemo.mvc1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MvcControllerDemo
 */
@WebServlet("/MvcControllerDemo")
public class MvcControllerDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MvcControllerDemo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * This is the MVC Controller
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// get data from model
		String[] students = {"Adnan", "Jack", "Farhana", "Jim"};
		
		// set values to pass to the view
		request.setAttribute("studentList",students);
		
		// set dispatcher
		RequestDispatcher dispatcher = request.getRequestDispatcher("/MvcViewDemo.jsp");
		
		// forward to the view
		dispatcher.forward(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
