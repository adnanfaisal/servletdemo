package com.adnan.servletdemo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ContextParamTestServlet
 */
@WebServlet("/ContextParamTestServlet")
public class ContextParamTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ServletContext context = getServletContext();
		String cartSize = context.getInitParameter("CartSize");
		String teamName = context.getInitParameter("ProjectTeamName");
		
		
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
	
		
		writer.println("<html> <body>");
		writer.println("<p> Cart size " + cartSize + " " );
		writer.println("<p> Team name " + teamName + " " );		
		writer.println("</body> </html>");
		
	}

}
